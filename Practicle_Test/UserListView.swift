//
//  ViewController.swift
//  Practicle_Test
//
//  Created by Hitendra on 15/09/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import UIKit
import CoreData

class UserListCustomCell : UITableViewCell{
    
    @IBOutlet weak var userImage : UIImageView?
    @IBOutlet weak var userName : UILabel?
    @IBOutlet weak var userEmail : UILabel?
    @IBOutlet weak var updateDate : UILabel?
    var indexPath : NSIndexPath?
    
    func configureCell(_ user : User){
        self.userName!.text = user.fullName;
        self.userEmail!.text = user.email
        self.updateDate!.text = user.updatedAt
        self.loadUserImage(user.profileUrl!)
    }
    
    
    private func loadUserImage(_ imageUrl : String){
       // self.userImage!.image = UIImage(named: "placeholder")
        WebServiceManager.sharead.downlodImage(imageUrl, complition: { (data, error) in
            if(error == nil){
                let image = UIImage(data: data! as Data)
                if(image == nil ){
                   // self.userImage!.image = UIImage(named: "placeholder")
                }
                else{
                    self.userImage!.image = image
                }
            }
        })
    }
}

class UserListView : UIViewController {

    var refreshControl = UIRefreshControl()
    @IBOutlet weak var tblUserList : UITableView!
    @IBOutlet weak var txtUserSearch : UITextField!
    let userListVM = UserListVM()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblUserList.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        userListVM.vc = self;
        tblUserList.tableFooterView = UIView()
        userListVM.loadUserList(isRefreshed: false)

    }
    @objc func refresh(_ sender: AnyObject) {
        userListVM.loadUserList(isRefreshed: true)
    }
}

extension UserListView: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sections = userListVM.fetchUserList.sections
        let sectionInfo = sections?[section]
        return sectionInfo?.numberOfObjects ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let CellIdentifier = "UserListCustomCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? UserListCustomCell
        cell!.userImage!.image = nil
        cell?.selectionStyle = UITableViewCell.SelectionStyle.none
        cell?.indexPath = indexPath as NSIndexPath
        let  user = (userListVM.fetchUserList.object(at: indexPath)) as User
        cell?.configureCell(user)
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

extension UserListView: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tblUserList.beginUpdates()
    }
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                tblUserList.insertRows(at: [indexPath], with: .automatic)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                tblUserList.deleteRows(at: [indexPath], with: .automatic)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                tblUserList.reloadRows(at: [indexPath], with: .automatic)
            }
            break;
        case .move:
            if let newIndexPath = newIndexPath {
                if let indexPath = indexPath {
                    tblUserList.deleteRows(at: [indexPath], with: .automatic)
                    tblUserList.insertRows(at: [newIndexPath], with: .automatic)
                }
            }
            break;
        default: break
        }
    }
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tblUserList.endUpdates()
    }
}

