//
//  User+CoreDataProperties.swift
//  Practicle_Test
//
//  Created by Hitendra on 15/09/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var address: String?
    @NSManaged public var createdAt: String?
    @NSManaged public var dateofbirth: String?
    @NSManaged public var designation: String?
    @NSManaged public var email: String?
    @NSManaged public var fullName: String?
    @NSManaged public var gender: String?
    @NSManaged public var phone: String?
    @NSManaged public var profileUrl: String?
    @NSManaged public var salary: Double
    @NSManaged public var updatedAt: String?
    @NSManaged public var userid: Int64

}
