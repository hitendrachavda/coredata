//
//  WebServiceManager.swift

import UIKit

class WebServiceManager: NSObject {
    
    static let sharead = WebServiceManager()
    
    func loadUsers(urlString : String, complition : @escaping( [String : Any]?, Error?) -> Void){
        
        let url = URL(string: urlString)
        guard let requestUrl = url else {
            fatalError()
        }
        let request = URLRequest(url: requestUrl)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                if error == nil{
                    if let responseDict = try? JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any] {
                        complition(responseDict,("Something went wrong" as! Error))
                    }
                }
                else{
                    complition(nil,error)
                }
            }
        }
        task.resume()
    }
}
