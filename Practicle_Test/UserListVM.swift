

import UIKit
import CoreData

class UserListVM: NSObject {
    
    let moc = DBManager.sharedInstance.mainMoc()
    weak var vc : UserListView?
    var searchString : String? = ""
    
    func loadUserList(isRefreshed : Bool){
        if(!isRefreshed){
            ActivityView.sharedInstance.showLoading(vc: vc!)
        }
        WebServiceManager.sharead.loadUsers(urlString: "https://beta3.moontechnolabs.com/app_practical_api/public/api/user") { (response, error) in
            ActivityView.sharedInstance.hideLoading(vc: self.vc!)
            self.vc?.tblUserList.refreshControl?.endRefreshing()
            if(error == nil){
                let userList = response!["data"] as! [[String : Any]]
                for userInfo in userList {
                    DBManager.sharedInstance.saveUser(userInfo) { (erroMsg) in
                    }
                }
                self.reloadList()
            }
            else{
                print(error?.localizedDescription as Any)
            }
            
        }
    }
    
    func reloadList(){
        self.searchString = ""
        self.resetUserList()
    }
    
    func searchKeyWord() -> NSPredicate? {
        var fieldWisePredicates: [AnyHashable] = []
        
        if(searchString!.count > 0){
            let unidquString = self.searchString! + "*"
            let currentDatePredicate: NSPredicate! = NSPredicate(format: "fullName LIKE[c] %@", unidquString as CVarArg)
            fieldWisePredicates.append(currentDatePredicate)
        }
        var finalPredicate: NSPredicate? = nil
        if let aPredicates = fieldWisePredicates as? [NSPredicate] {
            finalPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: aPredicates)
        }
        return finalPredicate
    }
    
    @objc public func resetUserList(){
        fetchUserList.fetchRequest.predicate = searchKeyWord()
        do {
            try fetchUserList.performFetch()
        } catch {
            let fetchError = error as NSError
            print("Unable to Perform Fetch Request")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        vc?.tblUserList.reloadData()
    }
    
    lazy var fetchUserList: NSFetchedResultsController<User> = {
        
        var fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        var entity = NSEntityDescription.entity(forEntityName: "User", in: moc)
        fetchRequest.entity = entity
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "fullName", ascending: true)]
        
        // Create Fetched Results Controller
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: nil)
        
        // Configure Fetched Results Controller
        fetchedResultsController.delegate = vc
        
        return fetchedResultsController as! NSFetchedResultsController<User>
    }()
    
}
