//
//  DBManager.swift


import Foundation
import UIKit
import CoreData

class DBManager : NSObject {
    
    static let sharedInstance = DBManager()
    
    func mainMoc() -> NSManagedObjectContext {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { fatalError("Could not convert delegate to AppDelegate") }
        return appDelegate.persistentContainer.viewContext
    }
    
    func getUser(predicate : NSPredicate) -> NSArray {
           let users = NSMutableArray()
           let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
           fetchRequest.predicate = predicate
           do {
               let results = try self.mainMoc().fetch(fetchRequest)
               for info in results {
                   users.add(info)
               }
           } catch  {
               return users
           }
           return users
       }
    
    func saveUser(_ userDict : [String : Any] , erroHandlar : (_ errorMsg : String) -> ()) {
        
        let predicate = NSPredicate(format: "userid = %@", userDict["id"] as! CVarArg)
        let user = self.getUser(predicate: predicate)
        if(user.count == 0){
            let user = NSEntityDescription.insertNewObject(forEntityName: "User", into: self.mainMoc()) as? User
            user?.address = (userDict["address"] as? String)
            user?.dateofbirth = (userDict["dob"] as? String)
            user?.designation = (userDict["designation"] as? String)
            user?.email = (userDict["email"] as? String)
            user?.fullName = (userDict["full_name"] as? String)
            user?.gender = (userDict["gender"] as? String)
            user?.phone = (userDict["phone"] as? String)
            user?.profileUrl = (userDict["profile_pic_url"] as? String)
            let salary = userDict["salary"] as? Double
            if((salary) != nil){
                user?.salary = salary!
            }
            else{
                user?.salary = 0
            }
            
            user?.userid = Int64((userDict["id"] as! Int))
            user?.createdAt = (userDict["created_at"] as? String)
            user?.updatedAt = (userDict["updated_at"] as? String)
        }
        self.saveContext()
        erroHandlar("")
    }

    func saveContext(){
        do {
            try self.mainMoc().save()
        } catch  {
            print("data is not saved")
        }
    }
    
    func deleteObject(_ object : NSManagedObject){
        self.mainMoc().delete(object)
        self.saveContext()
    }
    
    
}
